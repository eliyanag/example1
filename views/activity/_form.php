<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use app\models\Status;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoryId')->
		dropDownList(/* שם טבלה*/ Category::getCategories()) ?> 
		
	<?php if($model->isNewRecord){ ?>
			<div style="display:none;"> <?= $form->field($model, 'statusId')->textInput(['value'=>2]) ?> </div>
		<?php }else{ ?>
			<?= $form->field($model, 'statusId')-> dropDownList(Status::getStatuses()) ?>
	<?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
